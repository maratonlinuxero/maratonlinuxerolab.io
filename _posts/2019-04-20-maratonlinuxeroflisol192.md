---
layout: post
title: "#02 Maratón Linuxero FLISol 2019"
date: 2019-04-20
category: Podcast
youtube:
  id: ITMkIeoAKsw
  start:
    h: 1
    m: 1
    s: 32
podcast:
  audio: https://archive.org/download/MLflisol192/MLflisol192
tags: [audio, podcast, directo]
---
En la segunda parte de este evento, 3 sedes nuevas que inician su andadura en España conversan sobre los preparativos y expectativas.  
Jorge Lama desde A Coruña, Voro Mataix desde Valencia y Juan Febles desde Tenerife comentan cómo está saliendo adelante este proyecto.

Aquí tienes la emisión en YouTube:

{% include youtubePlayer.html %}

Y aquí el audio extraído:

{% include audioPlayer.html %}

Toda la música utilizada en Maratón linuxero tiene licencia Creative Commons:  
 
1. [Way to Success](https://www.jamendo.com/track/1334807/way-to-success) por [Addict Sound](https://www.jamendo.com/artist/451073/addict-sound)


Si quieres **contactar** con nosotros, puedes hacerlo de las siguientes formas:
