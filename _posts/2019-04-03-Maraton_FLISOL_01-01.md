---
layout: post
title: "Maratón Linuxero FLISOL 2019"
date: 2019-04-03
category: Noticias
tags: [FLISOL, maraton linuxero, maraton linuxero]
---

**Maratón Linuxero y FLISOL 2019, ¡Qué buena mezcla!**

Ya está aquí el FLISOL 2019, el principal evento de Instalación de Software Libre, que ya celebra su 15º Edición. El Proyecto Maratón Linuxero quiere rendir en una emisión especial un gran homenaje a todas esas personas que hacen posible que el FLISOL salga adelante, y además que traspase fronteras y se consolide como EL EVENTO de referencia del Software y la Cultura Libre.

El día **20 de Abril a partir de las 21:00 (UTC)**, justo una semana antes del comienzo del Festival, conoceremos de primera mano las opiniones, iniciativas y todo lo relacionado con el FLISOL por parte de algunos de los Organizadores Nacionales del evento: ¿Cómo te lo vas a perder?

Maratón Linuxero sigue adelante y nos gustaría que nos acompañases, te esperamos!

![#Prensa](/media/2019_FLISOL/01.jpg)
###### Obra realizada por el equipo de Maratón Linuxero (CC BY-SA 4.0)


Si quieres formar parte, colaborar o simplemente opinar y/o criticar, puedes ponerte en contacto con nosotros a través de los siguientes canales: