---
layout: post
title: "#03 Maratón Linuxero FLISol 2019"
date: 2019-04-20
category: Podcast
youtube:
  id: ITMkIeoAKsw
  start:
    h: 1
    m: 59
    s: 0
podcast:
  audio: https://archive.org/download/MLflisol193/MLflisol193
tags: [audio, podcast, directo]
---
Finalizamos esta emisión con una charla entre componentes de Maratón Linuxero para pedir perdón por la no emisión del 15 de diciembre de 2018 y comentar futuros eventos del proyecto.

Aquí tienes la emisión en YouTube:

{% include youtubePlayer.html %}

Y aquí el audio extraído:

{% include audioPlayer.html %}

Toda la música utilizada en Maratón linuxero tiene licencia Creative Commons:  
 
1. [Way to Success](https://www.jamendo.com/track/1334807/way-to-success) por [Addict Sound](https://www.jamendo.com/artist/451073/addict-sound)


Si quieres **contactar** con nosotros, puedes hacerlo de las siguientes formas:
